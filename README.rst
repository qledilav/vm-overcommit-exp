vm-overcommit-exp 
=====================================

Working principle
-----------------

Here an experiment is conducted as follow.

config.yml -> vagrant or g5k provider -> inventory -> 

1) Describe the experiment topology in config.yml (by default)
2) Request the g5k or vagrant provider for resources
   - g5k
     1) oarsub
     2) kadeploy3
3) Create an ansible inventory
4) Prepare the Physical Machines (PMs) to recieve Virtual Machines (VMs)
5) Install the software necessary to instanciate VMs
6) Obtain all non-contextualized data such as the disk image on which the VMs will be based
7) Create the VMs
8) Add VMs to the inventory
9) Install the software necessary for the experiment such as stress or iperf
10) Launch the experiment
11) Move the produced data somewhere
12) Redo things
- Goto 6)
- Destroy VMs to be able to go back to 4)
- Destroying PMs is equivalent to force a redeployment
13) Analyze data

Project structure
-----------------

- vm_overcommit_exp
  - conf.yaml :: describe the topology
  - vm_overcommit_exp
    - cli.py :: code the console interactions with your experiment
    - tasks :: code the tasks available for cli.py to be executed
    - ansible :: now we are in the world of ansible
      - site.yml :: describe how roles are assign to hosts
      - roles
        - pm
          - prepare
          - instantiate_vms
          - destroy
        - vm
          - prepare
        - temoin
          - fetch
          - stress
          - iperf :: and see shaker
        - groupe1
          - prepare
          - fetch
          - stress
          - iperf
        - groupe2
          - prepare
          - fetch
          - stress
          - iperf

- simple        :: make the conf.auto.yaml
- g5k           :: claim ressources from g5k
- inventory     :: make host
- vm_inventory  :: make v[0-9]* inventories
- prepare
  - 
  -       
  - vm_host_prepare :: install soft and data on vm_host vm_host/tasks/prepare
  - 
  -
# now on the virtual part ( no need to interact with the inventory made by inventory
- contextualize_vms                            pm/tasks/contextualize_vms
- instantiate_vms                              pm/tasks/instanciate_vms
- prepare_subjects                             subjects/tasks/prepare
- launch_exp                                   subjects/
- move
- launch_exp
- move
- launch_exp
- move
- launch_exp
- move
- destroy_any_v
