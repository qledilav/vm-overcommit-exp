from copy import deepcopy
from execo_g5k.api_utils import (get_host_cluster,
                                 get_site_hosts,
                                 get_host_attributes,
                                 get_cluster_hosts,
                                 get_cluster_site)
from dsl_parser import parse, tokenize

def dsl2config(elements, **kwargs):
    '''
    Make an enoslib_config from the dsl
    :param elements: n-tree with categories then subcat then dsl strings
    :param cluster: the used cluster
    :param site: used to find hosts if cluster is not defined
    '''
    o_l = []
    for e in elements:
        if isinstance(e, (str,)):
            o_l.append({
                "dsl_str": e.replace("\n"," "),
                "topo": parse(tokenize(e))})
        else:
            for subcat in e["subcategories"]:
                for topo in subcat["topologies"]:
                    o_l.append({
                        "dsl_str": topo.replace("\n", " "),
                        "topo": parse(tokenize(topo)),
                        "category": e["name"],
                        "subcat": subcat["name"]
                    })
    # the return-to-be object
    r = {
        "g5k":
        {
            "defaults": kwargs,
            "vtopologies": [],
            "resources":
            {
                "machines": [],
                "networks": []
            }
        }
    }
    total = []  # regroup all partial pms descriptor
    for o in o_l:
        hosts = get_site_hosts(kwargs["site"]) if "cluster" not in kwargs else get_cluster_hosts(kwargs["cluster"])
        l, partial = gen_hw_conf(o["topo"],
                                 hosts,
                                 kwargs.get("site", get_cluster_site(kwargs["cluster"])))
        total.append(partial)
        # add naming infos
        r["g5k"]["vtopologies"].append({
            "topo": l,
            "dsl_str": o["dsl_str"],
            "category": o["category"],
            "subcat": o["subcat"]})
    # TODO improve: for now network is considered identical everywhere
    r["g5k"]["resources"]["networks"] = total[0]["g5k"]["resources"]["networks"]
    r["g5k"]["resources"]["machines"] = _merge([e["g5k"]["resources"]["machines"] for e in total])
    return r


def _g5k2conf(host_name):
    tmp = get_host_attributes(host_name)
    return {"cluster": get_host_cluster(host_name),
         "primary_network": "n0",
         "roles": ["vm_holder"],
         "secondary_networks": [],
         "cores": tmp["architecture"]["nb_cores"]}


def gen_hw_conf(o, hosts, site):
    hardware = deepcopy(hosts)  # TODO filter who can't virt, take it as parameter
    machines = []
    l = []
    for pm in o: # rly dirty, anyone is welcomed here (to do better)
        tmp = {}  # the pm that will be put in vtopologies
        tmp["vms"] = pm["vms"]
        tmp["dupli"] = pm["dupli"]
        tmp["cores"] = pm["cores"]
        tmp["process"] = pm["process"]
        tmp["which"] = pm["which"]
        # get the good hardware configured for enoslib
        valid_hw = deepcopy(
            [
                _g5k2conf(e)
                for e in hardware
                if get_host_attributes(e)["architecture"]["nb_cores"] >= pm["cores"]
            ][0])
        # get the hardware corresponding to the last role if it exist
        if pm["roles"] != []:
            tmp_hw = [
                _g5k2conf(e) for e in hardware
                if get_host_cluster(e) == pm["roles"][-1]]
            if tmp_hw != []:
                valid_hw = deepcopy(tmp_hw[0])
        tmp["on_pm"] = valid_hw["cluster"]  # link pm to vm_holder
        valid_hw["nodes"] = pm["dupli"]
        valid_hw.pop("cores")  # not valid field in machine
        tmp["roles"] = pm["roles"]

        machines.append(valid_hw)
        l.append(tmp)
    return l, {
        "g5k":
        {
            "resources":
            {
                "machines": machines,
                "networks": [
                    {
                        "id": "n0",
                        "type": "prod",
                        "roles": ["prod_network"],
                        "site": site
                    },
                    {
                        "id": "n1",
                        "type": "slash_22",
                        "roles": ["sub_network"],
                        "site": site
                    }
                ]
            }
        }
    }


def _comp(a, b):
    '''
    compare physical machines
    '''
    return (a["cluster"] == b["cluster"]
        and a["primary_network"] == b["primary_network"]
        and a["roles"] == b["roles"])


def _add(l):
    '''
    add 2 compatible machines
    '''
    r = l[0]
    r["nodes"] = max([v["nodes"] for v in l])
    return r


def _merge(ll):
    '''
    merge physical machines
    '''
    ll2 = []
    for l in ll:
        tmp = []
        for a in l:
            found = False
            for b in tmp:
                if _comp(a, b):
                    found = True
                    b["nodes"] += a["nodes"]
                    break
            if not found:
                tmp.append(a)
        ll2.extend(tmp)

    ll3 = []
    for a in ll2:
        found = False
        for b in ll3:
            if _comp(a, b):
                found = True
                b["nodes"] = max(a["nodes"], b["nodes"])
                break
        if not found:
            ll3.append(a)

    return ll3








# def symplify_config(config):
#     m = config["g5k"]["machines"]
#     r = [m[0]]
#     count = [1]
#     tmp=[]
#     for i in range(len(m)-1):
#         found = False
#         for j in range(len(r)):
#             if m[i] == r[j]:
#                 count[j]+=1
