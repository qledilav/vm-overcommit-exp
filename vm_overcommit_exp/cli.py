import click
import logging
import yaml

import vm_overcommit_exp.tasks as t
from vm_overcommit_exp.constants import CONF

from dsl2config import dsl2config

logging.basicConfig(level=logging.DEBUG)


@click.group()
def cli():
    pass


def load_config(file_path):
    """
    Read configuration from a file in YAML format.
    :param file_path: Path of the configuration file.
    :return:
    """
    with open(file_path) as f:
        configuration = yaml.safe_load(f)
    return configuration

from pprint import pprint
@cli.command(help="claim ressources, generate inventory and detailled configuration from simple topology")
@click.option("-f", "b_file",
              is_flag=True,
              help="the string is a file")
@click.option("-o", "--out",
              default="conf.auto.yaml",
              help="the out file")
@click.option("--site",
              required=True,
              help="site name")
@click.argument("topo")
def dsl2conf(topo, b_file, out, site):
    if b_file:
        with open(topo) as f:
            topo = yaml.safe_load(f)
    config = dsl2config(topo,
                        site=site,
                        vm_primary_network="n1")
    with open(out, "w") as f:
        yaml.safe_dump(config, f)


@cli.command(help="Claim resources on Grid'5000 (frontend).")
@click.option("--force",
              is_flag=True,
              help="force redeployment")
@click.option("--conf",
              default=CONF,
              help="alternative configuration file")
@click.option("--env",
              help="alternative environment directory")
def g5k(force, conf, env):
    config = load_config(conf)
    t.g5k(force=force, config=config, env=env)


@cli.command(help="Claim resources on vagrant (localhost).")
@click.option("--force",
              is_flag=True,
              help="force redeployment")
@click.option("--conf",
              default=CONF,
              help="alternative configuration file")
@click.option("--env",
              help="alternative environment directory")
def vagrant(force, conf, env):
    config = load_config(conf)
    t.vagrant(force=force, config=config, env=env)


@cli.command(help="Generate the Ansible inventory [after g5k or vagrant].")
@click.option("--env",
              help="alternative environment directory")
def inventory(env):
    t.inventory(env=env)

@cli.command(help="Generate the Ansible inventory [after inventory].")
@click.option("--env",
              help="alternative environment directory")
def vm_inventory(env):
    t.vm_inventory(env=env)


@cli.command(help="Create a configuration file from an input string.\
\nlike: [(ooo)2 ()1 oo]2")
@click.option("--env",
              help="alternative environment directory")
@click.option("--vtopo", type=int,
              help="the choosed topo")
def test(env, vtopo):
    t.test(env=env)


@cli.command(help="Configure available resources [after deploy, inventory or\
             destroy].")
@click.option("--env",
              help="alternative environment directory")
def prepare(env):
    t.prepare(env=env)


@cli.command(help="Backup the deployed environment")
@click.option("--env",
              help="alternative environment directory")
def backup(env):
    t.backup(env=env)


@cli.command(help="Destroy the deployed environment")
@click.option("--env",
              help="alternative environment directory")
def destroy(env):
    t.destroy(env=env)


@cli.command(help="Claim resources from a PROVIDER and configure them.")
@click.argument("provider")
@click.option("--force",
              is_flag=True,
              help="force redeployment")
@click.option("--conf",
              default=CONF,
              help="alternative configuration file")
@click.option("--env",
              help="alternative environment directory")
def deploy(provider, force, conf, env):
    config = load_config(conf)
    t.PROVIDERS[provider](force=force, config=config, env=env)
    t.inventory()
    t.prepare(env=env)


@cli.command(help="")
@click.option("--env",
              help="alternative environment directory")
@click.option("-w", "--which_topo",
              default=0,
              help="which topo to use")
def contextualize_vms(env, which_topo):
    t.contextualize_vms(env=env, which_topo=which_topo)


@cli.command(help="")
@click.option("--env",
              help="alternative environment directory")
@click.option("-w", "--which_topo",
              default=0,
              help="which topo to use")
def instantiate_vms(env, which_topo):
    t.instantiate_vms(env=env, which_topo=which_topo)


@cli.command(help="")
@click.option("--env",
              help="alternative environment directory")
@click.option("-w", "--which_topo",
              default=0,
              help="which topo to use")
def destroy_vms(env, which_topo):
    t.destroy_vms(env=env, which_topo=which_topo)


@cli.command(help="")
@click.option("--env",
              help="alternative environment directory")
@click.option("-w", "--which_topo",
              default=0,
              help="which topo to use")
def prepare_subjects(env, which_topo):
    t.prepare_subjects(env=env, which_topo=which_topo)


@cli.command(help="")
@click.option("--env",
              help="alternative environment directory")
@click.option("-w", "--which_topo",
              default=0,
              help="which topo to use")
def exp(env, which_topo):
    t.exp(env=env, which_topo=which_topo)


@cli.command(help="do the tests in https://github.com/simgrid/simgrid/blob/master/teshsuite/msg/cloud-sharing/cloud-sharing.c")
@click.option("--force",
              is_flag=True,
              help="force redeployment")
@click.argument("which_topologies",
                type=int,
                nargs=-1)
@click.option("--env",
              help="alternative environment directory")
@click.option("--site",
              required=True,
              help="alternative site")
def all(force, which_topologies, env, site):
    temoin = "temoin( oo )2"  # get a baseline?
    with open('random.txt','w') as f:
        click.secho("aaa", file=f, bold=True, bg='magenta')
    1/0
    with open("simgrid_vm_tests.yaml") as f:
        tests = yaml.safe_load(f)
    config = dsl2config(tests,
                        site=site,
                        vm_primary_network="n1")
    with open("conf.auto.yaml", "w") as f:
        yaml.safe_dump(config, f)
    t.g5k(force=force, config=config, env=env)
    t.inventory()  # generate the inventory from the g5k provider
    generated_topologies = t.vm_inventory(env=env)  # TODO see with msimonin why it can't return without modifing enolib # generate virtual inventory on top of the usual
    t.prepare(env=env)  # prepare the physical setup
    if len(which_topologies) == 0:
        which_topologies = tuple(range(len(generated_topologies)))
    with click.progressbar(which_topologies) as wts:
        for which_topo in wts:
            t.contextualize_vms(env=env, which_topo=which_topo)  # define vms
            t.instantiate_vms(env=env, which_topo=which_topo)  # vms state=running
            t.prepare_subjects(env=env, which_topo=which_topo)  # prepare all subjects for the tests
            t.exp(env=env, which_topo=which_topo)  # launch processes and monitor ressources consumption of each processes
            t.destroy_vms(env=env, which_topo=which_topo)

@cli.command(help="do stats on data")
@click.argument("which_topologies",
                type=int,
                nargs=-1)
@click.option("--env",
              help="alternative environment directory")
def stats(which_topologies, env):
    t.stats(env=env, which_topologies=which_topologies)


@cli.command(help="do plot on data")
@click.argument("which_topologies",
                type=int,
                nargs=-1)
@click.option("--env",
              help="alternative environment directory")
def plot(which_topologies, env):
    t.plot(env=env, which_topologies=which_topologies)


import os
from multiprocessing import Pool
from dateutil.parser import parse
import datetime, time
from glob import glob


def time_stats_on_exp(ltl):
    preparation, topos = zip(*ltl)
    try:
        import pandas as pd
        print('stats on the preparation\n%s'
              % pd.DataFrame(list(preparation)).describe())
        print('stats on the topologies\n%s'
              % pd.DataFrame(sum(topos, [])).describe())
    except:
        print('pandas seams to be missing to show detailed stats')
        print('mean time for preparation\n%s'
              % (sum(preparation, datetime.timedelta(0)) / len(preparation)))
        print('mean time for the topologies\n%s'
              % (sum(sum(topos, []), datetime.timedelta(0))
                 / len(sum(topos, []))))
        raise


def protocole_exec(args):  # TODO catch exceptions to keep the influence of a particular bug limited
    t_topo = []
    t_reserv = datetime.timedelta(0)
    if 'job_name' not in args["enos_conf"]["g5k"]:
        args["enos_conf"]["g5k"]["job_name"] = "enoslib%s_%d" % (time.time(), args["i"])
    click.secho('%s will be the jobname'
                % args["enos_conf"]["g5k"]["job_name"],
                bg='magenta', fg='white', bold=True, color=True)
    for j in range(args["n_times"]):
        t_try_res_start = datetime.datetime.today()
        click.secho(
            '>===============>> Start deploying at %s >>=================>'
            % (t_try_res_start),
            bg='cyan', fg='black', color=True)
        env = os.path.join(args["batches_id"] + '_%d_%d_%s'
                           % (args["i"], j, t_try_res_start.isoformat()))
        os.mkdir(env)
        if j > 0:  # else it will reserve again
            origin_s = os.path.join(args["batches_id"] + '_%d_%d_*'
                                    % (args["i"], 0))
            with open(os.path.join(glob(origin_s)[0], 'env'), 'r') as src, \
                 open(os.path.join(env, 'env'), 'w') as dest:
                dest.write(src.read())
        # g5k provider # create an enoslib_env each time it's called
        t.g5k(force=args["force"],
              config=args["enos_conf"],
              env=env)
        # generate the inventory from the g5k provider
        t.inventory(env=env)
        # generate virtual inventory on top of the usual
        generated_topologies = t.vm_inventory(env=env)
        # prepare
        t.prepare(env=env)  # prepare the physical setup to receive vms
        t_try_res_end = datetime.datetime.today()
        click.secho(
            '<===============<< Deployed at %s        <<=================<'
            % (t_try_res_end),
            bg='cyan', fg='black', color=True)
        t_reserv += (t_try_res_end - t_try_res_start) / args["n_times"]

        # Some logic to choose the executed tpopologies
        if len(args["which_topologies"]) == 0:
            which_topologies = tuple(range(len(generated_topologies)))
        else:
            which_topologies = args["which_topologies"]

        # Now sequentially execute each topology
        with click.progressbar(which_topologies,
                               label='Topologies') as wts:
            for k, which_topo in enumerate(wts):
                t_topo_start = datetime.datetime.today()
                click.secho(
                    '>>---> Starting topology %s at %s >>---------------->'
                    % (which_topo, t_topo_start.isoformat()),
                    bg='white', fg='blue', color=True)
                try:
                    t.destroy_vms(env=env, which_topo=which_topo)
                    t.contextualize_vms(env=env, which_topo=which_topo)  # define vms
                    t.instantiate_vms(env=env, which_topo=which_topo)  # vms state=running
                    t.prepare_subjects(env=env, which_topo=which_topo)  # prepare all subjects for the tests
                    t.exp(env=env, which_topo=which_topo,
                          cores_lim=True,
                          exec_duration=args["exec_duration"],
                          moni_or_not=args["moni_or_not"])  # launch processes and monitor ressources consumption of each processes
                except Exception as e:
                    click.secho('topology %s failed with %s at %s\n'
                                % (which_topo, e, datetime.datetime.today().isoformat()),
                                bg='white', fg='red', blink=True, color=True)
                    print(args['i'], t_reserv, t_topo)
                    raise
                else:
                    click.secho(
                        '<---<< Ending topology %s without troubles at %s <-<<'
                        % (which_topo, datetime.datetime.today().isoformat()),
                        bg='white', fg='green', color=True)
                finally:
                    t.cleanup_subjects(env=env, which_topo=which_topo)
                    t.destroy_vms(env=env, which_topo=which_topo)

                t_topo_end = datetime.datetime.today()
                if j == 0:
                    t_topo.append(datetime.timedelta(0))
                t_topo[k] += (t_topo_end - t_topo_start) / args["n_times"]
    print(args['i'], t_reserv, t_topo)
    return t_reserv, t_topo

@cli.command(help="Automated exp")
@click.option("--force",
              is_flag=True,
              help="force redeployment between batch")
@click.option("--cluster",
              required=True,
              help="the cluster where the experience is made")
@click.option("--resa",
              default=None,
              help="the choosed resevation jobname, else for recovering, but no parallel")
@click.option("-p", "--parallel",
              default=1,
              help="the number of experience made in parallel")
@click.option("-n", "--n_times",
              default=1,
              help="the number of time the experience is made")
@click.option("-d", "--exec_duration",
              default=60,
              help="the duration of the workloads")
@click.option('--moni', 'moni_or_not', flag_value='--moni',
              default=True)
@click.option('--no-moni', 'moni_or_not', flag_value='--no-moni')
@click.option("--walltime",
              default="03:00:00",
              help="the walltime of one iteration, thus we take n_time*walltime as the walltime for the resevation")
@click.argument("which_topologies",
                type=int,
                nargs=-1)
def automated_exp(force, cluster, parallel, resa, exec_duration, moni_or_not,
                  n_times, walltime, which_topologies):
    with open(CONF, 'r') as meta_conf_f:
        meta_conf = yaml.safe_load(meta_conf_f)
    hidden_s = os.path.join(os.path.dirname(CONF), '.%s' % os.path.basename(CONF))

    # Verify if the conf file have been changed between exp
    try:
        with open(hidden_s, 'r') as h_f:
            hidden = yaml.safe_load(h_f)
    except IOError:
        # doesn't exist
        with open(hidden_s, 'w') as h_f:
            yaml.safe_dump(meta_conf, h_f)
    else:
        # exists
        if meta_conf != hidden:
            while True:
                if click.confirm("""The config file have been changed,
                you need to archive the preceding experiments (git branch? cp . ?)
                then remove the hidden conf file"""):
                    return

    batches_id = '.venosEnv_%s' % time.time()
    # gen enos conf
    enos_conf = dsl2config(meta_conf,
                           cluster=cluster,
                           vm_primary_network="n1",
                           moni_or_not=moni_or_not,
                           exec_duration=str(exec_duration),
                           force=force)

    time_by_user = parse(walltime) - parse('00:00:00')
    walltime = time_by_user * n_times
    MAX_WALLTIME = datetime.timedelta(hours=12)
    if walltime.days > 0 or walltime.seconds > MAX_WALLTIME.seconds:
        raise BaseException("You can't take more than " + str(MAX_WALLTIME))
    click.secho('%s of walltime will be reserved' % walltime,
                bg='blue', fg='white', bold=True, color=True)
    enos_conf["g5k"]["walltime"] = str(walltime)

    # not ready to launch parallelle executions
    if parallel > 1:
        click.secho('parrallel is in dev and desn\'t work',
                    bg='red', fg='white', bold=True, blink=True, color=True)

    if resa is not None:
        enos_conf["g5k"]["job_name"] = resa
        parallel = 1
    enos_conf["g5k"]["parameters"] = {
        'force': force,
        'which_topologies': which_topologies,
    }
    dataset = []
    for i in range(parallel):
        dataset.append({
            'i': i,
            'n_times': n_times,
            'enos_conf': enos_conf,
            'which_topologies': which_topologies,
            'force': force,
            'batches_id': batches_id,
            'moni_or_not': moni_or_not,
            'exec_duration': exec_duration
        })
    print(time_stats_on_exp(
        [protocole_exec(dataset[0])]))
    # poolprocess = parallel
    # chunksize = 1
    # pool = Pool(processes=poolprocess)
    # result = pool.map(protocole_exec, dataset, chunksize)
    # print(time_stats_on_exp(
    #     result))
    return 0
