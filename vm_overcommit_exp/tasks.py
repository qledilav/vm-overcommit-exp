from enoslib.api import generate_inventory, run_ansible
from enoslib.task import enostask
from enoslib.infra.enos_g5k.provider import G5k
from enoslib.infra.enos_vagrant.provider import Enos_vagrant
import logging
import os

from vm_overcommit_exp.constants import ANSIBLE_DIR

logger = logging.getLogger(__name__)

def init_provider(provider, name, force, config, env):
    print("---------------------")
    print(name)
    print("--------------")
    instance = provider(config[name])
    print("--------------")
    roles, networks = instance.init(force_deploy=force)
    print("--------------")
    env["config"] = config
    env["roles"] = roles
    env["networks"] = networks


@enostask(new=True)
def g5k(**kwargs):
    init_provider(G5k, "g5k", **kwargs)


@enostask(new=True)
def vagrant(**kwargs):
    init_provider(Enos_vagrant, "vagrant", **kwargs)

import pprint


@enostask()
def inventory(**kwargs):
    env = kwargs["env"]
    roles = env["roles"]
    networks = env["networks"]
    env["inventory"] = os.path.join(env["resultdir"], "hosts")
    print(pprint.pprint(env["config"]["g5k"]))
    generate_inventory(roles, networks, env["inventory"], check_networks=True)


from enoslib.host import Host
from copy import deepcopy
from netaddr import EUI, IPAddress
import re
import errno
from execo_g5k.api_utils import get_host_cluster
@enostask()
def vm_inventory(**kwargs):
    '''
    Generate the virtual inventories
    TODO integrate it in enoslib
    :param uniq: generate vms with unique name and mac if True, otherwise reset for each topo
    :param env: path to environment
    '''
    env = kwargs["env"]  # the environment, gathered by the enostask decorator
    uniq = kwargs.get("uniq", False)
    networks = env["networks"]  # list of networks
    vtopologies = env["config"]["g5k"]["vtopologies"]  # list of vtopologies
    default_vm_network = env["config"]["g5k"]["defaults"].get("vm_primary_network", "n1")

    # make the vtopologies dirs
    env["vinventories"] = []
    vinvent_dir = os.path.join(env["resultdir"],
                               "vtopologies/")
    os.makedirs(vinvent_dir)
    for i, t in enumerate(vtopologies):
        cat_dir = os.path.join(
            vinvent_dir,
            t["category"].replace(' ', '_'))
        try: os.makedirs(cat_dir)
        except: pass
        t_dir = os.path.join(
            cat_dir,
            t["subcat"].replace(' ', '_'))
        try: os.makedirs(t_dir)
        except: pass
        t_dir_dir = os.path.join(t_dir, 'vt' + str(i))
        os.makedirs(t_dir_dir)
        env["vinventories"].append(
            os.path.join(t_dir_dir, "hosts")
        )

    # make the vroles
    env["vroles"] = []
    id = 0
    avail_addr = deepcopy(env["networks"])
    vroles = [{} for a in range(len(vtopologies))]
    for i in range(len(vtopologies)):  # each virtual topo
        if not uniq:  # reset ressources for each topo
            id = 0
            avail_addr = deepcopy(env["networks"])

        # make a list of available pms
        avail_pms = deepcopy(env["roles"]["vm_holder"])
        # make the pms and vms who will go in vroles
        vroles[i]["pms"] = []
        vroles[i]["vms"] = []
        for pm in vtopologies[i]["topo"]:  # loop on pms, keep pm order
            for dupli in range(pm['dupli']):
                # get a pm from available pms
                print("avail_pms")
                print(avail_pms)
                for j, e in enumerate(avail_pms):
                    if get_host_cluster(e.address) == pm["on_pm"]:
                        pm_host = avail_pms.pop(j)
                        break
                    # the following forbid cluster name as alias
                    elif e.alias == pm["on_pm"]:
                        pm_host = avail_pms.pop(j)
                        break
                else:
                    raise Exception('no pm coressponding to %s' % pm["on_pm"])
                pm_host = deepcopy(pm_host)
                pm_host.extra["vms"] = []  # needed to instanciate vms
                pm_host.extra["process"] = pm["process"]  # needed to launch workloads
                pm_host.extra["cores"] = pm["cores"]  # needed to limit pm cores
                # vroles[i][name]=[] # !!!! inventory groupe and host of same name -> bugs

                for vm in pm["vms"]:  # loop over vms
                    for dupli in range(vm['dupli']):
                        # get the principal network
                        net = avail_addr[int(vm.get("primary_network",
                                                    default_vm_network)[1:])]
                        if int(EUI(net["start"][1])) > int(EUI(net["end"][1])):
                            raise Exception('no more addr for vms')
                        # a vm that will be processed by generate_inventory
                        host = Host(
                            net["start"][0],
                            alias="vm" + str(id),
                            user="root",
                            extra={
                                "mac": net["start"][1],
                                "process": vm["process"],
                                "vcpu": vm["vcores"]
                            })
                        pm_host.extra["vms"].append("vm" + str(id))
                        id += 1
                        # increment the lower boundary of the ip/mac range
                        net["start"] = (
                            str(IPAddress(int(IPAddress(net["start"][0])) + 1)),
                            str(EUI(int(EUI(net["start"][1])) + 1)).replace('-', ':'))

                        vroles[i]["vms"].append(host)
                        for k in vm["roles"]:  # add Host in each roles
                            if k not in vroles[i]:
                                vroles[i][k] = []
                            vroles[i][k].append(host)

                vroles[i]["pms"].append(pm_host)
                for k in pm["roles"]:  # add Host in each roles
                    if k not in vroles[i]:
                        vroles[i][k] = []
                    vroles[i][k].append(pm_host)

        env["vroles"].append(vroles[i])
        generate_inventory(vroles[i],
                           networks,
                           env["vinventories"][i],
                           check_networks=False)
    return env["vinventories"]


@enostask()
def prepare(**kwargs):
    '''
    Prepare vm holders to receive vms
    '''
    env = kwargs["env"]
    extra_vars = {
        "enos_action": "prepare",
        "enos_playbook": "vm_holders"
    }
    run_ansible([os.path.join(ANSIBLE_DIR, "site.yml")],
                env["inventory"], extra_vars=extra_vars)


@enostask()
def backup(**kwargs):
    env = kwargs["env"]
    extra_vars = {
        "enos_action": "backup",
        "enos_playbook": "vm_holder"
    }
    run_ansible([os.path.join(ANSIBLE_DIR, "site.yml")],
                env["inventory"], extra_vars=extra_vars)


@enostask()
def destroy(**kwargs):
    env = kwargs["env"]
    extra_vars = {
        "enos_action": "destroy",
        "enos_playbook": "vm_holder"
    }
    run_ansible([os.path.join(ANSIBLE_DIR, "site.yml")],
                env["inventory"], extra_vars=extra_vars)


@enostask()
def test(**kwargs):
    '''in dev!'''
    env = kwargs["env"]
    extra_vars = {
        "enos_action": "test"
    }
    run_ansible([os.path.join(ANSIBLE_DIR, "exp1.yml")],
                env["vinventories"][1], extra_vars=extra_vars)


# prepare # soft + non-contextualized data
@enostask()
def contextualize_vms(**kwargs):
    '''
    Define vms

    :param env: the environment
    :param which_topo: the choosed vtopology
    '''
    env = kwargs["env"]
    v_which = kwargs.get("which_topo", 0)
    extra_vars = {
        "v_action": "contextualize_vms",
        "enos_playbook": "role_pm"
    }
    run_ansible([os.path.join(ANSIBLE_DIR, "site.yml")],
                env["vinventories"][v_which], extra_vars=extra_vars)


# contextualization + instanciation # need to choose the vtopo
@enostask()
def instantiate_vms(**kwargs):
    '''
    Runs vms

    :param env: the environment
    :param which_topo: the choosed vtopology
    '''
    env = kwargs["env"]
    v_which = kwargs.get("which_topo", 0)
    extra_vars = {
        "v_action": "instantiate_vms",
        "enos_playbook": "role_pm"
    }
    run_ansible([os.path.join(ANSIBLE_DIR, "site.yml")],
                env["vinventories"][v_which], extra_vars=extra_vars)


@enostask()
def destroy_vms(**kwargs):
    env = kwargs["env"]
    v_which = kwargs.get("which_topo", 0)
    extra_vars = {
        "v_action": "destroy_vms",
        "enos_playbook": "role_pm"
    }
    run_ansible([os.path.join(ANSIBLE_DIR, "site.yml")],
                env["vinventories"][v_which], extra_vars=extra_vars)


# # here vms are ssh-able
# prepare test subjects # need to choose the vtopo too
# # here vms are ssh-able
# prepare test subjects # need to choose the vtopo too
@enostask()
def prepare_subjects(**kwargs):
    '''
    Prepare subjects for the experiment,
    install software needed to launch workloads and monitor them
    require: instantiate_vms

    :param env: the environment
    :param which_topo: the choosed vtopology
    '''
    env = kwargs["env"]
    v_which = kwargs.get("which_topo", 0)
    extra_vars = {
        "v_action": "prepare",
        "enos_playbook": "role_subject"
    }
    run_ansible([os.path.join(ANSIBLE_DIR, "site.yml")],
                env["vinventories"][v_which],
                extra_vars=extra_vars)
@enostask()
def cleanup_subjects(**kwargs):
    '''
    Cleanup subjects for the experiment,
    require: instantiate_vms

    :param env: the environment
    :param which_topo: the choosed vtopology
    '''
    env = kwargs["env"]
    v_which = kwargs.get("which_topo", 0)
    extra_vars = {
        "v_action": "cleanup",
        "enos_playbook": "role_subject"
    }
    run_ansible([os.path.join(ANSIBLE_DIR, "site.yml")],
                env["vinventories"][v_which],
                extra_vars=extra_vars)


# exp  # need to choose the vtopo too
@enostask()
def exp(**kwargs):
    '''
    Launch workloads and monitor them
    required: prepare_subjects

    :param env: the environment
    :param which_topo: the choosed vtopology
    '''
    env = kwargs["env"]
    v_which = kwargs.get("which_topo", 0)
    exec_duration = kwargs.get("exec_duration", 60)
    moni_or_not = kwargs.get("moni_or_not", '--moni')
    extra_vars = {
        "v_action": "execute",
        "v_data_out_dir": os.path.dirname(env["vinventories"][v_which]),
        "enos_playbook": "role_subject",
        "exec_duration": exec_duration,
        "moni_or_not": moni_or_not
    }
    run_ansible([os.path.join(ANSIBLE_DIR, "site.yml")],
                env["vinventories"][v_which],
                extra_vars=extra_vars)

import glob

@enostask()
def stats(**kwargs):
    '''
    stats on data
    '''
    import pandas as pd
    env = kwargs["env"]
    v_which = kwargs["which_topologies"]
    env_dir = env["resultdir"]
    for i, p in enumerate(env["vinventories"]):
        if len(v_which) != 0 and i not in v_which:
            continue
        vtopo_dir = os.path.dirname(p)
        # tmp_dir = os.path.dirname(p)
        # dir_in_v_topo = []
        # for j in range(3):
        #     tmp_dir, tmp_name = os.path.split(tmp_dir)
        #     dir_in_v_topo.append(tmp_name)
        # out_dir = os.path.join(env_dir, "stats", *reversed(dir_in_v_topo))
        print(os.path.join(vtopo_dir, 'processed_data'))
        # processed_data/* # each process by position in the dsl, 0 pour le premier, n pour le n-ieme
        # processed_data/*/* # redeploye
        # processed_data/*/*/* # non-redeploye
        # processed_data/*/*/*/log # le csv tq
        # dt(ms) dt_cpu(ms)
        # 124 0.2
        # 134 1.57
        # 171 1.77
        # 132 2.07
        # 124 1.54
        for force_exp in glob.glob(vtopo_dir + '/results/*'):  # TODO put '/raw_data/*'
            for node_path in glob.glob(force_exp + '/*'):
                for no_force_exp in glob.glob(node_path + '/data_exp/default'):  # TODO put '/data_exp/*'
                    with open(os.path.join(no_force_exp, 'id_pid_cmd.asso'), 'r') as asso_f:
                        for line in asso_f.read().splitlines():
                            if line == '':
                                continue
                            p_id, pid, cmd = line.split(' ', 2)
                            print(p_id, pid, cmd)
                            with open(os.path.join(no_force_exp, pid + '.log'), 'r') as log:
                               print(log.read().splitlines()[0])


PROVIDERS = {
    "g5k": g5k,
    "vagrant": vagrant,
#    "static": static
#    "chameleon": chameleon
}
