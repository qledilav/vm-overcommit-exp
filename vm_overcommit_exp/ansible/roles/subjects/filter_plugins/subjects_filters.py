#!/usr/bin/python
class FilterModule(object):
    def filters(self):
        return {
            'to_chained': self.filter1,
            'get': self.filter2
        }

    def filter1(self, s_l, path=None, d=None):
        if path is None and d is None:
            mem = {}
            s = []
            for e in s_l:
                if e in mem:
                    mem[e] += 1
                else:
                    mem[e] = 0
                s.append(e + str(mem[e]))
            return ' '.join(s)
        else:
            mem = {}
            s = ""
            for e in s_l:
                if e in mem.keys():
                    mem[e] += 1
                else:
                    mem[e] = 0
                tmp = e + str(mem[e])
                s += d[e] + " >> " + path + tmp + " & "
            return s + " wait"

    def filter2(self, d, l):
        return [d[e] for e in l]
