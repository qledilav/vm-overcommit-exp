#!/usr/bin/env python
import os
import sys


def set_cpu(cpu_l, state):
    for i in cpu_l:
        tmp = os.path.join("/","sys", "devices", "system", "cpu", "cpu" + str(i), "online")
        with open(tmp, "w") as f:
            f.write(str(state))


total = int(os.sysconf("SC_NPROCESSORS_CONF"))

wanted = int(sys.argv[1])
if wanted == 0:
    set_cpu(range(1, total), "1")  # activate all cpu
else:
    set_cpu(range(wanted, total), "0")  # deactive all cpu we don't want
    set_cpu(range(1, wanted), "1")  # make sure that the others are activated
