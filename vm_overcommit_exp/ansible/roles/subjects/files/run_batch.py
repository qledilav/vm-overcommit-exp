#!/usr/bin/python

import datetime

#import subprocess

import psutil  # $ pip install psutil

import time

import os
import sys

# inputs
# run_batch.py dir/ --no-moni 60 o0 o1 o2 a1 a2 o3
directory = sys.argv[1]
exec_duration = 60
start_para = 4
monitoring = True
if len(sys.argv) > 2 and sys.argv[2] == "--no-moni":
    monitoring = False
    exec_duration = int(sys.argv[3])
elif len(sys.argv) > 2 and sys.argv[2] == "--moni":
    monitoring = True
    exec_duration = int(sys.argv[3])
else:
    start_para = 2
id_l = sys.argv[start_para:]

# end of inputs

processes = []
files_out = []

d = {}

with open("/tmp/alias2cmd.txt") as f:
    for l in f:
        tmp = l.split(' ', 1)
        d[tmp[0]] = tmp[1]

l = id_l
for i in range(len(l)):
    l[i] = {"id": l[i], "cmd": d[l[i][:1]]}

timeout = exec_duration + 5 # some more time allowed
starting_date = datetime.datetime.now()

try:
    import libvirt
except:
    libvirt_avail = False
else:
    libvirt_avail = True
    conn = libvirt.open('qemu:///system')

    domainIDs = conn.listDomainsID()
    doms = [conn.lookupByID(i) for i in domainIDs]

    d_doms = {d.name(): (d, open(directory + "%s.vms" % d.name(), "w") ) for d in doms}



for e in l:
    #    f_out = os.tmpfile()
    f_out = open(directory + e["id"] + ".out", "w")
    p = psutil.Popen(
        e["cmd"].format(directory + e['id'], str(exec_duration)), stdout=f_out, shell=True)
    p.nice(0)  # ensure the niceness level to default
    processes.append(p)
    files_out.append(f_out)


def get_proc_stats(p, detail=2):
    '''
    stats are not localized for now
    '''
    if detail == 3:
        r = p.as_dict([
            "name", "cpu_times", "memory_info", "cpu_percent", "cpu_num",
            "cmdline"
        ])
        r["total_times"] = p.cpu_times().user
        r["child_cpu_num"] = []
        for child in p.children(recursive=True):
            r["child_cpu_num"].append(child.cpu_num())
            r["total_times"] += child.cpu_times().user
        return r

    elif detail == 2:
        date = datetime.datetime.now()
        tmp = p.cpu_times()
        r = [tmp.user + tmp.system]
        for child in p.children(recursive=True):
            try:
                tmp = child.cpu_times()
                r[0] += tmp.user + tmp.system
            except:
                pass
        dt = datetime.datetime.now() - date
        return ' '.join([str(e) for e in [date.isoformat(), dt] + r])


runner_process = psutil.Process()

def kill_tree(p):
    if p.poll() is None:
        for child in p.children(recursive=True):
            child.kill()
        p.kill()


def c_function(l_p):
    '''
    Get recources taken by the listed processes
    The kind which can be greatly improved by a C code
    On mega overcommit it crumble
    :l_p: list of Process, when interfaced with c it would be a list of pid
    '''
    file_runner = open(directory + "%s.moni" % runner_process.pid, "w")
    files_log = [open(directory + "%s.log" % p.pid, "w") for p in l_p]
    l_p_pid = [p.pid for p in l_p]
    while True:
        for i in range(len(l_p)):
            if l_p[i].poll() is None:
                tmp = str(get_proc_stats(l_p[i]))
                files_log[i].write(tmp + '\n')

        # monitor the runner
        date = datetime.datetime.now()
        tmp = runner_process.cpu_times()
        runner_t_cpu = tmp.user + tmp.system
        for child in runner_process.children():
            if child.pid not in l_p_pid:
                try:
                    tmp = child.cpu_times()
                    runner_t_cpu += tmp.user + tmp.system
                except:
                    pass
                for child in child.children(recursive=True):
                    try:
                        tmp = child.cpu_times()
                        runner_t_cpu += tmp.user + tmp.system
                    except:
                        pass
        dt = datetime.datetime.now() - date
        file_runner.write("%s %s %s\n" % (date.isoformat(), dt, runner_t_cpu))
        # end of monitor the runner
        # monitor the vms
        if libvirt_avail:
            for k, (d, f) in d_doms.items():

                stats = d.getCPUStats(True)
                date = datetime.datetime.now()
                f.write("%s %s %s %s\n" % (date.isoformat(),
                                           stats[0]['cpu_time'],
                                           stats[0]['system_time'],
                                           stats[0]['user_time']))
        # end of monitor the vms

        tmp = datetime.datetime.now() - starting_date
        if tmp.total_seconds() > timeout:
            for p in l_p:
                kill_tree(p)
            time.sleep(5)
            break
        time.sleep(0.1)

    for f in files_log:
        f.close()
    file_runner.close()


if monitoring:
    c_function(processes)
else:
    loops = (exec_duration + 5) // 8
    if libvirt_avail:
        for i in range(loops):
            for k, (d, f) in d_doms.items():
                stats = d.getCPUStats(True)
                date = datetime.datetime.now()
                f.write("%s %s %s %s\n" % (date.isoformat(),
                                           stats[0]['cpu_time'],
                                           stats[0]['system_time'],
                                           stats[0]['user_time']))
            time.sleep(8)  # one measure every ~8 seconds
    else:
        time.sleep(exec_duration+5)

    for p in processes:
        kill_tree(p)
    time.sleep(5)


if libvirt_avail:
    for k, (d, f) in d_doms.items():
        f.close()
    conn.close()
#for p, f in processes:
#    print(p)
#    p.wait()
#    p.terminate()

for i, f in enumerate(files_out):
    #    f.seek(0)
    #    logfile.write("-----------------" + str(i) + "-----------------")
    #    logfile.write(f.read())
    f.close()

with open(directory + "id_pid_cmd.asso", "w") as f:
    for i, p in enumerate(processes):
        f.write(l[i]["id"] + " " + str(p.pid) + " " + l[i]["cmd"] + "\n")

