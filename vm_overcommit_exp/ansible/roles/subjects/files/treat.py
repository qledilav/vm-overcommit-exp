import sys
import datetime

with open(sys.argv[1]) as f:
    d_l = []
    for l in f:
        day, time, cpu_time = l.split(' ')
        d_l.append(
            datetime.datetime.strptime(day + ' ' + time,
                                       "%Y-%m-%d %H:%M:%S.%f")
            )
    tmp1 = d_l[1:]
    tmp2 = d_l[:-1]
    tmp_r = []
    for i in range(len(tmp1)):
        tmp_r.append((tmp1[i] - tmp2[i]).total_seconds())

    print(tmp_r)

import numpy as py

print(py.mean(tmp_r))
print(py.var(tmp_r))
