#!/usr/bin/python
class FilterModule(object):
    def filters(self):
        return {
            'get': self.filter1,
            'another_filter': self.b_filter
        }

    def filter1(self, d, l):
        return [d[e] for e in l]

    def b_filter(self, a_variable):
        a_new_variable = a_variable + 'B CRAZY NEW FILTER'
        return a_new_variable