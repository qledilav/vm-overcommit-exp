from re import VERBOSE

from funcparserlib.lexer import make_tokenizer, Token, LexerError
from funcparserlib.parser import (some, a, maybe, many, finished, skip,
                                  forward_decl, with_forward_decls,
                                  NoParseError)

# just thinking but it might be cool to use pandoc


def tokenize(string):
    """str -> Sequence(Token)"""
    specs = [
        ('Space', ('[ \t\r\n]+', )),
        ('Integer', ('''
            -?                  # Minus
            (0|([1-9][0-9]*))   # Int
            ''', VERBOSE)),
        ('Named_begin_[',
         ('(?:[A-Za-z_][A-Za-z_0-9]*(?:\,[ \t\r\n]*[A-Za-z_0-9]*)*)*\[', )),
        ('Named_begin_(',
         ('(?:[A-Za-z_][A-Za-z_0-9]*(?:\,[ \t\r\n]*[A-Za-z_0-9]*)*)?\(', )),
        ('Op', ('[\(\)\[\]]', )),
        ('Letter', ('[A-Za-z_]', )),
        ('String', ('[A-Za-z_][A-Za-z_0-9]*', )),
    ]
    useless = ['Space']
    t = make_tokenizer(specs)
    return [x for x in t(string) if x.type not in useless]


# test the tokenizer
# print("\n".join(str(a) for a in tokenize("( [ooo]2 o )2")))


def parse(tokens):
    '''Sequence(Token) -> object

    A parser to set up the experiments on VMs easily'''

    const = lambda x: lambda _: x
    tokval = lambda x: x.value
    toktype = lambda t: some(lambda x: x.type == t) >> tokval
    op = lambda s: a(Token('Op', s)) >> tokval
    n = lambda s: a(Token('Letter', s)) >> tokval
    value = forward_decl()

    def make_named(string):
        'str -> str[]'
        tmp = string[:-1]
        if tmp == '':
            return []
        else:
            return [a.strip() for a in tmp.split(',')]

    def make_integer(tok):
        'Token -> int'
        return int(tok.value)

    integer = (some(lambda tok: tok.type == 'Integer') >> make_integer)
    named_begin_v = toktype('Named_begin_[') >> make_named
    named_begin_p = toktype('Named_begin_(') >> make_named
    process = some(lambda x: x.type == 'Letter') >> tokval

    VM = (
        maybe(integer)
        + named_begin_v
        + many(process)
        + op(']')
        + integer
        >> (lambda x: {
            'roles': x[1],
            'process': x[2:-2][0],
            'vcores': x[-1],
            'dupli': 1 if x[0] is None else x[0]})
    )

    def mixed_to_splited(l):
        which = []
        vms = []
        process = []
        for e in l:
            if isinstance(e,str):
                which.append('p')
                process.append(e)
            else:
                which.append('m')
                vms.append(e)
        return {
            'which': which,
            'vms': vms,
            'process': process
        }


    inPM = with_forward_decls(
        lambda:
        (process + inPM >> (lambda x: [x[0]] + x[1]))
        | (VM + inPM >> (lambda x: [x[0]] + x[1]))
        | (VM >> (lambda x: [x]))
        | (process >> (lambda x: [x])))

    PM = (
        maybe(integer)
        + named_begin_p
        + maybe(inPM)
        + op(')')
        + integer
        >> (lambda x: {
            'roles': x[1],
            'cores': x[-1],
            'dupli': 1 if x[0] is None else x[0],
            **(mixed_to_splited(x[2]))}))

    glob = many(PM)

    value.define(integer | process | VM | PM | glob)

    dsl = glob + skip(finished)
    out = dsl.parse(tokens)
    return out


# import json

# print(json.dumps(
#     parse(tokenize("( ooo [oooo]3 ooo [oooo]1 ooo )30 ( [oooo]9 [oooo]7)2")),
#     sort_keys=True,
#     indent=4))

#     if isinstance(a,list)|isinstance(a,tuple):
#         for b in a:
#             if isinstance(b,list)|isinstance(b,tuple):
#                 for c in b:
#                     if isinstance(c,list)|isinstance(c,tuple):
#                         for d in c:
#                             print(" "*16+str(d))
#                     else:
#                         print(" "*12+str(c))
#             else:
#                 print(" "*8+str(b))
#     else:
#         print(" "*4+str(a))
# else:
#     print(" "*1+str(0000000000))
